import { Component, OnInit } from '@angular/core';
import {Input} from '@angular/core';
import {PostService} from '../services/post.service';



@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  @Input() index: number;
  @Input() id: number;
  @Input() title: string;
  @Input() content: string;
  @Input() created_at: Date;

  constructor(private postservice: PostService) {}

  ngOnInit() {}

  getColorPost(id: number) {
    if ( this.postservice.getNumberOfLove(id) > 0) {
      return 'green';
    } else if (this.postservice.getNumberOfLove(id) < 0) {
      return 'red';
    } else {
      return 'black';
    }
  }

  loveIt(id: number) {
    this.postservice.loveIt(id);
  }

  dontLoveIt(id: number) {
    this.postservice.dontLoveIt(this.id);
  }

  getNumberOfLove(id: number) {
    return this.postservice.getNumberOfLove(id);
  }

  deleteOnePost(index: number) {
    this.postservice.removePost(+index);
  }


}
