import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';


@Injectable()
export class PostService {

  private posts = [];
  postsSubject = new Subject<any[]>();


  constructor(private httpClient: HttpClient, private router: Router) {
    this.getPosts();
    this.initializeData();
  }

   addPost(title: string, content: string, loveIts: number) {
    const postsObject = {
      id: 0,
      title: '',
      content: '',
      created_at: new Date(),
      loveIts: 0
    };
    postsObject.content = content;
    postsObject.title = title;
    postsObject.loveIts = loveIts;
    if (this.posts.length === 0) {
      postsObject.id = 0;
      console.log('generate id zero : ' + 0);
    } else {
      const lent = this.posts[(this.posts.length - 1)].id + 1;
      console.log('generate id  : ' + lent);
      postsObject.id = this.posts[(this.posts.length - 1)].id + 1;
    }
    this.posts.push(postsObject);
    this.trier();
    this.savePosts();
  }

  emitPosts() {
    this.postsSubject.next(this.posts.slice());
  }


  savePosts() {
    this.httpClient
      .put('https://firebasblog-56e3d.firebaseio.com/posts.json', this.posts)
      .subscribe(
        () => {
          console.log('Enregistrement terminé !');
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
    this.emitPosts();
  }

  getPosts() {
    this.httpClient
      .get<any[]>('https://firebasblog-56e3d.firebaseio.com/posts.json')
      .subscribe(
        (response) => {
          console.log('response:' + response);
          if ( response) {
            this.posts = response;
          }
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
    this.emitPosts();
  }



  createNewPosts(title: string, content: string, loveIts: number) {
    this.addPost(title, content, loveIts);
  }

  removePost(index: number) {
    this.posts.splice(+index, 1);
    this.emitPosts();
    this.savePosts();
  }

  loveIt(id: number) {
    const postIndexTo = this.getPostsById(id);
    return postIndexTo.loveIts++;
  }

  getPostsById(id: number) {
      const post = this.posts.find(
        (s) => {
          return s.id === id;
        }
      );
      return post;
  }


  dontLoveIt(id: number) {
    const postIndexTo = this.getPostsById(id);
    if ( postIndexTo) {
      return postIndexTo.loveIts--;
    } else {
      return 0;
    }
  }

  getNumberOfLove(id: number) {
    const postIndexTo = this.getPostsById(id);
    if (postIndexTo) {
      return postIndexTo.loveIts;
    } else {
      return 0;
    }
  }

  trier() {
    if ( this.posts) {
      this.posts = this.posts.sort(function (obj1, obj2) {
           return obj1.id - obj2.id;
      });
      this.emitPosts();
    }
  }

  initializeData() {
    if (this.posts === undefined || this.posts === null || !this.posts || this.posts.length === 0) {
      this.addPost('Mon premier post', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ac egestas est, quis blandit enim. Nulla non ipsum sed magna placerat elementum. Sed dictum tristique quam, in congue nunc ultrices quis. Proin tincidunt', 1);
      this.addPost('Mon deuxieme post', 'Suspendisse imperdiet, leo ac aliquam feugiat, nibh turpis gravida quam, sit amet blandit velit purus in sem. Curabitur felis lacus, blandit non enim consectetur, ornare porttitor quam. Proin et sollicitudin augue', -1);
      this.addPost('Encore un post', 'Donec ut lacinia magna. Proin efficitur purus sit amet vestibulum vehicula. Praesent nec sem eget leo pellentesque laoreet. Nam laoreet, lacus id vehicula vulputate, ante mi elementum risus,', 0);
      this.savePosts();
      this.getPosts();
      this.trier();
    }
  }



}
