import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {PostService} from '../services/post.service';


@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.scss']
})

export class NewPostComponent implements OnInit {

  newPostForm: FormGroup;
  errorMessage: string;

  constructor(private formBuilder: FormBuilder, private postservice: PostService, private router: Router) {
    this.postservice.getPosts();
  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.newPostForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      content: ['', [Validators.required]]
    });
  }

  onSubmit() {
    const title = this.newPostForm.get('title').value;
    const content = this.newPostForm.get('title').value;
    this.postservice.createNewPosts(title, content, 0);
    this.router.navigate(['/posts']);
  }

}
