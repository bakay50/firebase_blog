import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {PostService} from '../services/post.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit, OnDestroy {


  posts: any[];
  postsSubscription: Subscription;
  constructor(private postservice: PostService) { }

  ngOnInit() {
    this.postsSubscription = this.postservice.postsSubject.subscribe(
      (posts: any[]) => {
        this.posts = posts;
      }
    );
    this.postservice.trier();
    this.postservice.emitPosts();
  }

  ngOnDestroy() {
    this.postsSubscription.unsubscribe();
  }





}
